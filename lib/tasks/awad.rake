require 'csv'
namespace :awad do
  desc "pull pokemon data"
  task seed_awad: :environment do



    CSV.foreach("lib/assets/pokemon affinity table.csv", :headers =>true, encoding: 'iso-8859-1:utf-8') do |row |
      puts row.inspect #just so that we know the file's being read

      #create new model instances with the data
      Pokemon.create!(
      name: row[0],
      affinity: row[1]
      
    )
    end
    
    CSV.foreach("lib/assets/Pokemon Atk.csv", :headers =>true, encoding: 'iso-8859-1:utf-8') do |row |
      puts row.inspect #just so that we know the file's being read

      #create new model instances with the data
      Pokemonatkvalue.create!(
      attack: row[0],
      pokemon_id: row[1]
      
    )
    end
  end

end
