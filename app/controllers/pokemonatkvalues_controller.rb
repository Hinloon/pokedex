class PokemonatkvaluesController < ApplicationController
  before_action :set_pokemonatkvalue, only: [:show, :edit, :update, :destroy]

  # GET /pokemonatkvalues
  # GET /pokemonatkvalues.json
  def index
    @pokemonatkvalues = Pokemonatkvalue.all
  end

  # GET /pokemonatkvalues/1
  # GET /pokemonatkvalues/1.json
  def show
  end

  # GET /pokemonatkvalues/new
  def new
    @pokemonatkvalue = Pokemonatkvalue.new
  end

  # GET /pokemonatkvalues/1/edit
  def edit
  end

  # POST /pokemonatkvalues
  # POST /pokemonatkvalues.json
  def create
    @pokemonatkvalue = Pokemonatkvalue.new(pokemonatkvalue_params)

    respond_to do |format|
      if @pokemonatkvalue.save
        format.html { redirect_to @pokemonatkvalue, notice: 'Pokemonatkvalue was successfully created.' }
        format.json { render :show, status: :created, location: @pokemonatkvalue }
      else
        format.html { render :new }
        format.json { render json: @pokemonatkvalue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pokemonatkvalues/1
  # PATCH/PUT /pokemonatkvalues/1.json
  def update
    respond_to do |format|
      if @pokemonatkvalue.update(pokemonatkvalue_params)
        format.html { redirect_to @pokemonatkvalue, notice: 'Pokemonatkvalue was successfully updated.' }
        format.json { render :show, status: :ok, location: @pokemonatkvalue }
      else
        format.html { render :edit }
        format.json { render json: @pokemonatkvalue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pokemonatkvalues/1
  # DELETE /pokemonatkvalues/1.json
  def destroy
    @pokemonatkvalue.destroy
    respond_to do |format|
      format.html { redirect_to pokemonatkvalues_url, notice: 'Pokemonatkvalue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pokemonatkvalue
      @pokemonatkvalue = Pokemonatkvalue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pokemonatkvalue_params
      params.require(:pokemonatkvalue).permit(:attack, :pokemon_id)
    end
end
