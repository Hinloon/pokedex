json.extract! pokemonatkvalue, :id, :attack, :pokemon_id, :created_at, :updated_at
json.url pokemonatkvalue_url(pokemonatkvalue, format: :json)
