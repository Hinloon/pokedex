json.extract! pokemon, :id, :name, :affinity, :created_at, :updated_at
json.url pokemon_url(pokemon, format: :json)
