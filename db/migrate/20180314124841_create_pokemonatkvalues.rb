class CreatePokemonatkvalues < ActiveRecord::Migration[5.1]
  def change
    create_table :pokemonatkvalues do |t|
      t.integer :attack
      t.integer :pokemon_id

      t.timestamps
    end
  end
end
