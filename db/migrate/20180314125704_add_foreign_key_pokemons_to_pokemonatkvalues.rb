class AddForeignKeyPokemonsToPokemonatkvalues < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :pokemonatkvalues, :pokemons
  end
end
