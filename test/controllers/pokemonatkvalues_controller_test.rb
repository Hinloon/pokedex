require 'test_helper'

class PokemonatkvaluesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pokemonatkvalue = pokemonatkvalues(:one)
  end

  test "should get index" do
    get pokemonatkvalues_url
    assert_response :success
  end

  test "should get new" do
    get new_pokemonatkvalue_url
    assert_response :success
  end

  test "should create pokemonatkvalue" do
    assert_difference('Pokemonatkvalue.count') do
      post pokemonatkvalues_url, params: { pokemonatkvalue: { attack: @pokemonatkvalue.attack, pokemon_id: @pokemonatkvalue.pokemon_id } }
    end

    assert_redirected_to pokemonatkvalue_url(Pokemonatkvalue.last)
  end

  test "should show pokemonatkvalue" do
    get pokemonatkvalue_url(@pokemonatkvalue)
    assert_response :success
  end

  test "should get edit" do
    get edit_pokemonatkvalue_url(@pokemonatkvalue)
    assert_response :success
  end

  test "should update pokemonatkvalue" do
    patch pokemonatkvalue_url(@pokemonatkvalue), params: { pokemonatkvalue: { attack: @pokemonatkvalue.attack, pokemon_id: @pokemonatkvalue.pokemon_id } }
    assert_redirected_to pokemonatkvalue_url(@pokemonatkvalue)
  end

  test "should destroy pokemonatkvalue" do
    assert_difference('Pokemonatkvalue.count', -1) do
      delete pokemonatkvalue_url(@pokemonatkvalue)
    end

    assert_redirected_to pokemonatkvalues_url
  end
end
