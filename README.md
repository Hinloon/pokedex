# README
This Pokdex application uses open data from https://www.kaggle.com/rounakbanik/pokemon to represent the number of pokemon that have a certain affinity. The data was also used to 
display the top 15 highest attack Pokemon.
The application makes use of User log-ins, Charts and tables complete with search function, and a live twitter feed of the offical Pokemon Twitter feed.

How to run:
Locally
  Register with Cloud 9 and download Pokedex file.
  cd to pokedex
  bundle install to install all required gems
  rails db:migrate to run the database migration
  rake awad:seed_awad to parse the data from the CSV files in lib/assets folder
  rails s -b $IP -p $PORT
  Push the preview button located at the top of the page.
  
Opening with Heroku
  Navigate to the Pokedex directory
  use the following commands:
  heroku login - log in to your heroku account
  heroku create - creates an empty application
  git oush heroku master - pushes the code from master branch to heroku remote
  heroku run rake db:migrate - migrates database
  heroku run rake awad:seed_awad - parse data from CSV file
  heroku open - launch application on heroku server
  
The application can be accessed from the following url: https://awad-pokemons.herokuapp.com/