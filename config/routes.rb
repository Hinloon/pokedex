Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'
  get 'charts/columnchart'
  get 'charts/barchart'
  get 'startup/index'
  post '/signup',  to: 'users#create'
  
  root 'startup#index'
  
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  resources :customers
  resources :pokemonatkvalues
  resources :pokemons
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
